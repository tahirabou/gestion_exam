<?php
$db_username = 'root';        
$db_password = '';            
$db_name     = 'gestion_exam'; 
$db_host     = 'mysql';       

// Connexion à la base de données
$db = mysqli_connect($db_host, $db_username, $db_password, $db_name);

// Vérification de la connexion
if (!$db) {
    die('Erreur de connexion à la base de données : ' . mysqli_connect_error());
}
?>
